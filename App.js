import React from 'react';
import { StyleSheet} from 'react-native';
import ListScreen from './screens/ListScreen'

import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import playerReducer from './store/reducers/player';

const rootReducer = combineReducers({
  player: playerReducer
});

const store = createStore(rootReducer);

export default function App() {

  return (
    <Provider store={store}>
      <ListScreen />
    </Provider>
  );
}

const styles = StyleSheet.create({

 
});
