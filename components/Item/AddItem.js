import React, {useState} from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity} from 'react-native';
import { useDispatch } from 'react-redux';
import Slider from '@react-native-community/slider';
import AButton from '../UI/AButton';

import * as playerAction from '../../store/actions/player';



const AddItem = ({submitHandler}) => {
    const dispatch = useDispatch();

    const [text, setText] = useState('');
    const [value, setValue] = useState(18);

    const changeTextHandler = (val) => {
        setText(val);
    }

    const valueChangeHandler = (value) => {
        setValue(value);
    }
    
    return (
        <View style={styles.container}>
            <View style={styles.inputContainer}>
                <TextInput
                    style={styles.inputText}
                    onChangeText={changeTextHandler}
                    placeholder='Please Enter Full Name'
                />
                <AButton 
                    title={'Save'}
                    style={styles.abutton} 
                    toPress={() => dispatch(playerAction.addPlayer(text, value))}
                />
            </View>
            <View style={styles.sliderContainer}>
                <View style={styles.displayAgeContainer}>
                    <Text>Age: {value}</Text>
                </View>
                <Slider
                    style={styles.slider}
                    minimumValue={1}
                    maximumValue={120}
                    minimumTrackTintColor="#FF0000"
                    maximumTrackTintColor="#0000FF"
                    step="1"
                    value={value}
                    onValueChange={value => setValue(value)}
                />
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    inputContainer: {
        flexDirection: 'row',
        marginLeft: 20
    },
    input: {
        marginBottom: 10,
        paddingHorizontal: 8,
        paddingVertical: 6,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
    },
    inputText: {
        marginTop: 16,
        borderColor: '#bbb',
        borderWidth: 1,
        borderRadius: 16,
        width: 300,
        height: 40,
        paddingLeft: 10,
    },
    abutton: {
        width: 80,
        height: 40,
        borderWidth: 1,
        borderColor: '#bbb',
        marginTop: 15,
        marginLeft: 5,
        borderWidth: 1,
        overflow: 'hidden',
        borderRadius: 10
      },
      sliderContainer:{
        flexDirection: 'row',
      },
      slider: {
        width: 310, 
        height: 40, 
        marginLeft: 20
      },
      displayAgeContainer: {
        marginTop: 12,
        marginLeft: 20
      }

});
export default AddItem;
