import React, {useState} from 'react'
import { StyleSheet, Text, Button, View, TouchableOpacity } from 'react-native';
import AButton from '../UI/AButton';
import * as playerAction from '../../store/actions/player';

import { useDispatch } from 'react-redux';



const List = ({item}) => {
    const dispatch = useDispatch();
    const [deleteButton, setDeleteButon] = useState(false);
    const displayDeleteButton = () => {
        setDeleteButon(!deleteButton)
    }
    return (
        <TouchableOpacity onLongPress={() => displayDeleteButton()} style={styles.list}>
                <View style={styles.titleContainer}>
                    {/* title */}
                    <Text style={styles.name}>{item.text}</Text>
                    <Text style={styles.age}>Age: {item.age}</Text>
                </View>
                    {/* button */}
                    {deleteButton ?
                    <AButton  
                        toPress={() => dispatch(playerAction.removePlayer(item.key))}
                        title={'Delete'}
                        textColor={'white'}
                        style={styles.abutton}
                    /> : <View></View>}
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    titleContainer:{
        width: 300, 
    },
    list : {
        flexDirection: 'row',
        borderColor: '#828282',
        borderBottomWidth: 1,
        paddingBottom: 20,
        paddingLeft: 25,
        paddingTop: 20
      },
      name: {
        fontSize: 18,
        fontWeight: 'bold'
      },
      abutton: {
        width: 80,
        height: 40,
        borderWidth: 1,
        borderColor: '#FF3333',
        backgroundColor: '#FF3333',
        borderWidth: 1,
        overflow: 'hidden',
        borderRadius: 10
      },
})
export default List;