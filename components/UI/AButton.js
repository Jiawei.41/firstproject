import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';

const AButton = props => {
  return (            
        <View style={{...styles.buttonContainer, ...props.style}}>
            <TouchableOpacity onPress={props.toPress}  style={styles.button}>
                <Text style={{fontSize: props.textSize, 
                                 color: props.textColor,
                fontWeight: props.textWeight}}>{props.title}</Text>
            </TouchableOpacity>
        </View>
        )
};

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainer: {

    
},
});

export default AButton;
