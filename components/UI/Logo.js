import React from 'react'
import { StyleSheet, Image, View } from 'react-native'

const Logo = (props) => {
    return (
        <View style={styles.header}>
            <Image
            style={styles.tinyLogo}
            source={require('../../assets/ant-tech-logo.png')}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    header: {
        paddingTop: 50,
        alignItems: 'center'
    },
    tinyLogo: {
        width: 160,
        height: 90,
      },
})
export default Logo;