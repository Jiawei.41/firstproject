export const ADD_PLAYER = 'ADD_PLAYER';
export const REMOVE_PLAYER = 'REMOVE_PLAYER';

export const addPlayer = (name, age) => {
  return {
    type: ADD_PLAYER,
    playerData: { text: name,  age: age }
  };
};
export const removePlayer = (key) => {
  return {
    type: REMOVE_PLAYER,
    playerData: { key: key}
  };
};
