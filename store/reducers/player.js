import { ADD_PLAYER, REMOVE_PLAYER } from '../actions/player';



const initialState = {
  players: [
    {text: 'Kylian Mbappe',  age: '19', key: '1'},
    {text: 'Lionel Messi', age: '33', key: '2'},
    {text: 'Matthjs de Light', age: '20', key: '3'},
    {text: 'Marcus Rashford', age: '22', key: '4'},
    {text: 'Cristiano Ronaldo', age: '34', key: '5'},
    {text: 'Mohamed Salah', age: '29', key: '6'},

  ]
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_PLAYER:
      return {
        ...state,
        players: [...state.players, {text: action.playerData.text, age: action.playerData.age, key: Math.random().toString()}],
      }
    case REMOVE_PLAYER:
      return {
        ...state,
        players: state.players.filter(pls => pls.key != action.playerData.key),
      }
  }
  return state;
  
};
