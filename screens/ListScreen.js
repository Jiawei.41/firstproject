import React, {useState} from 'react';
import {  View, FlatList } from 'react-native';
import AddItem from '../components/Item/AddItem';
import ListItem from '../components/Item/ListItem'
import Logo from '../components/UI/Logo';

import { useSelector } from 'react-redux';

const ListScreen = () => {
    const players = useSelector(state => state.player.players);

    const [todos, setTodos] = useState([
        {text: 'Kylian Mbappe',  age: '19', key: '1'},
        {text: 'Lionel Messi', age: '33', key: '2'},
        {text: 'Matthjs de Light', age: '20', key: '3'},
        {text: 'Marcus Rashford', age: '22', key: '4'},
        {text: 'Cristiano Ronaldo', age: '34', key: '5'},
        {text: 'Mohamed Salah', age: '29', key: '6'},
    
      ]);
    
    return (
        <View>
            <Logo/>
            <View>
                <AddItem/>
                <View>
                    <FlatList
                    data={players}
                    renderItem={({item}) => (
                    <ListItem  item={item} /> 
                    )}
                    />
                </View>
            </View>
        </View>
    )
}

export default ListScreen;


